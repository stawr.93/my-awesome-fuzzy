export interface IChartData {
    datasets: IDataSet[];
}

export interface IDataSet {
    label: string;
    data: Point[];
    lineTension?: number;
}

export interface Point {
    x: number;
    y: number;
}