import { Component } from '@angular/core';
import { IChartData, IDataSet, Point } from './chart';

@Component({
    selector: 'home',
    template: require('./home.component.html')
})

export class HomeComponent {
    public chartType = 'line';
    public chartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        scales: {
            xAxes: [{
                type: 'linear',
                position: 'bottom'
            }]
        }
    };

    public MeanSquareError: number = 0;
    public AbsAverageError: number = 0;
    public MaxError: number = 0;    

    public datas: IChartData[];

    public xConfig: InputParamConfig;
    public yConfig: InputParamConfig;
    private zConfig: InputParamConfig;

    public rulesTable: IRule[];
    public observeTable: IObserveItem[];

    public termsNumber: number;
    public funcType: FunctionType;
    public observeIntervalNum: number;

    public get funcTypes(): { title: string, type: FunctionType }[] {
        return [
            { title: "Triangular", type: FunctionType.Triangular },
            { title: "Gaussian", type: FunctionType.Gaussian },
            { title: "Trapezoid", type: FunctionType.Trapezoid },
            { title: "Parabolic", type: FunctionType.Parabolic }
        ];
    }

    public get configs(): InputParamConfig[] {
        return [this.xConfig, this.yConfig];
    }

    constructor() {
        this.termsNumber = 5;
        this.observeIntervalNum = 1;
        this.funcType = FunctionType.Triangular;
        this.xConfig = {
            name: "X",
            minValue: -2,
            maxValue: 2
        }

        this.yConfig = {
            name: "Y",
            minValue: -2,
            maxValue: 2
        }

        this.zConfig = {
            name: "Z",
            minValue: -0.5,
            maxValue: 2
        }

        this.update();
    }

    public update() {
        this.datas = this.configs.concat(this.zConfig).map(cfg => this._getChart(cfg));
        this._updateRules();
        this._updateObservingTable();

        this.MeanSquareError = this._getMSE();
        this.AbsAverageError = this._getAAE();
        this.MaxError = this._getME();
    }

    private _getChart(paramConfig: InputParamConfig): IChartData {
        var max = paramConfig.maxValue;
        var min = paramConfig.minValue;
        var termsNum = this.termsNumber;
        var step = (max - min) / (termsNum - 1);
        var funcType = this.funcType;

        var datasets: IDataSet[] = [];

        for (let i = 0; i < termsNum; i++) {
            var dataSet: IDataSet = {
                label: `${paramConfig.name}#${i + 1}`,
                data: this._getDataForPlot(min, max, step, i, funcType),
            };

            if (funcType == FunctionType.Triangular || funcType == FunctionType.Trapezoid) {
                dataSet.lineTension = 0;
            }

            datasets.push(dataSet);
        }

        return {
            datasets: datasets
        };
    }

    private _getDataForPlot(min: number, max: number, step: number, termNum: number, funcType: FunctionType): Point[] {
        if (funcType == FunctionType.Triangular) {
            return this._getTriangularData(min, max, step, termNum);
        }

        if (funcType == FunctionType.Trapezoid) {
            return this._getTrapezoidData(min, max, step, termNum);
        }

        if (funcType == FunctionType.Parabolic) {
            return this._getParabolicData(min, max, step, termNum);
        }

        if (funcType == FunctionType.Gaussian) {
            return this._getGaussianData(min, max, step, termNum);
        }
    }

    private _getTriangularData(min: number, max: number, step: number, termNum: number): Point[] {
        var start: number;
        var end: number;
        var partOfStep = step / 2 + step / 7;

        var center: number = min + step * termNum;

        var result: Point[] = []

        if (termNum != 0) {
            start = center - partOfStep;
            result.push({ x: start, y: 0 });
        }

        result.push({ x: center, y: 1 });

        if (termNum != this.termsNumber - 1) {
            end = center + partOfStep;
            result.push({ x: end, y: 0 });
        }

        return result;
    }

    private _getParabolicData(min: number, max: number, step: number, termNum: number): Point[] {
        var start: number;
        var end: number;
        var partOfStep = step / 2 + step / 7;

        var center: number = min + step * termNum;
        start = center - partOfStep;
        end = center + partOfStep;

        var b = (end - start) / 2;
        var a = start + b;

        var result: Point[] = []

        var smallStep = (a + b - (a - b)) / 7;

        var f = (x: number) => 1 - Math.pow(((x - a) / b), 2);

        for (var currentX = (a - b); currentX <= (a + b); currentX += smallStep) {
            if (termNum === 0 && currentX < center) {
                currentX = center;
            }

            var shouldBreak = false;

            if (termNum === this.termsNumber - 1 && currentX > center) {
                currentX = center;
                shouldBreak = true;
            }

            var y = f(currentX);
            result.push({ x: currentX, y: y });

            if (shouldBreak) {
                break;
            }
        }

        if (termNum < this.termsNumber - 1 && result[result.length - 1].y > 0.0000001) {
            result.push({ x: a + b, y: 0 });
        }

        return result;
    }

    private _getGaussianData(min: number, max: number, step: number, termNum: number): Point[] {
        var start: number;
        var end: number;
        var partOfStep = step / 2 + step / 7;

        var center: number = min + step * termNum;
        start = center - partOfStep;
        end = center + partOfStep;

        var result: Point[] = []

        var smallStep = (max - min) / ((max - min) * 5);

        var f = (x: number) => Math.exp(-1 * Math.pow((x - center) / partOfStep, 2));

        for (var currentX = min; currentX <= max; currentX += smallStep) {
            var y = f(currentX);
            result.push({ x: currentX, y: y });
        }

        return result;
    }

    private _getTrapezoidData(min: number, max: number, step: number, termNum: number): Point[] {
        var start: number;
        var end: number;
        var partOfStep = step / 2 + step / 7;

        var center: number = min + step * termNum;
        var center1: number = center - step / 5;
        var center2: number = center + step / 5;

        var result: Point[] = []

        if (termNum != 0) {
            start = center - partOfStep;
            result.push({ x: start, y: 0 });
            result.push({ x: center1, y: 1 });
        } else {
            result.push({ x: center, y: 1 });
        }

        if (termNum != this.termsNumber - 1) {
            result.push({ x: center2, y: 1 });
            end = center + partOfStep;
            result.push({ x: end, y: 0 });
        } else {
            result.push({ x: center, y: 1 });
        }

        return result;
    }

    private _targetFunc(x: number, y: number): number {
        var result = x * Math.cos(x) + y * Math.sin(y);
        if (result < -0.5) {
            return -0.5;
        }

        if (result > 2) {
            return 2;
        }

        return result;
    }

    private _updateRules() {
        this.rulesTable = [];
        var termsNumber = this.termsNumber;

        for (var i = 1; i <= termsNumber; i++) {
            for (var j = 1; j <= termsNumber; j++) {
                var x = i;
                var y = j;
                var z = this._targetFunc(x, y);

                this.rulesTable.push({
                    X: x,
                    Y: y,
                    Z: z
                });
            }
        }
    }

    private _updateObservingTable() {
        this.observeTable = [];

        var stepByX = (this.xConfig.maxValue - this.xConfig.minValue) / (this.observeIntervalNum)
        var stepByY = (this.yConfig.maxValue - this.yConfig.minValue) / (this.observeIntervalNum)

        var x = this.xConfig.minValue;
        for (var i = 0; i < this.observeIntervalNum + 1; i++) {
            var y = this.yConfig.minValue;
            
            for (var j = 0; j < this.observeIntervalNum + 1; j++) {
                var z = this._targetFunc(x, y);
                var F_x = this._fx(x, y);
                
                this.observeTable.push({
                    X: x, 
                    Y: y,
                    Z: z,
                    F_x: F_x
                });

                y += stepByY;
            }

            x += stepByX;
        }
    }

    private _fx(x: number, y: number): number {
        var sum = 0; // числитель
        var sum2 = 0; // знаменатель;

        for(var i = 0; i < this.rulesTable.length; i++) {
            var yFunc = this._getY_func(i);
            var xFunc = this._getX_func(i);

            console.log(yFunc);
            console.log(xFunc);
            console.log(`i: ${i}`);
            console.log(`xFunc(${x}): ${xFunc(x)}`);
            console.log(`yFunc(${y}): ${yFunc(y)}`);
            console.log('===========================')

            sum += this.rulesTable[i].Z * xFunc(x) * yFunc(y);
            sum2+= xFunc(x) * yFunc(y);
        }

        console.log(sum2);

        return sum/sum2;
    }

    private _getX_func(termNumber: number): (arg: number) => number {
        return this._getFunc(termNumber, this.xConfig);
    }

    private _getY_func(termNumber: number): (arg: number) => number {
        return this._getFunc(termNumber, this.yConfig);
    }

    private _getFunc(termNumber: number, config: InputParamConfig): (arg: number) => number {
        if (this.funcType == FunctionType.Triangular) {
            return this._getTriangleFunc(termNumber, config);
        }

        if (this.funcType == FunctionType.Trapezoid) {
            return this._getTrapezoidFunc(termNumber, config);
        }

        if (this.funcType == FunctionType.Parabolic) {
            return this._getParabaloidFunc(termNumber, config);
        }

        if (this.funcType == FunctionType.Gaussian) {
            return this._getGaussianFunc(termNumber, config);
        }

        throw Error("Unknown func");
    }

    private _getTriangleFunc(termNumber: number, params: InputParamConfig): (x: number) => number {
        var min = params.minValue;
        var max = params.maxValue;
        var step = this._getStepSize(min, max);
        var partOfStep = step / 2 + step / 7;
        var center: number = min + step * termNumber;
        var left: number = center - partOfStep;
        var right: number = center + partOfStep;

        return (x: number) => {
            if(left <= x && x < center){
                return (x - left) / (center - left);
            } else if(center <= x && x < right) {
                return (x - right) / (center - right);
            } else {
                return 0;
            }
        };
    }

    private _getTrapezoidFunc(termNumber: number, params: InputParamConfig): (x: number) => number {
        var min = params.minValue;
        var max = params.maxValue;
        var step = this._getStepSize(min, max);
        var partOfStep = step / 2 + step / 7;

        var center: number = min + step * termNumber;
        var center1: number = center - step / 5;
        var center2: number = center + step / 5;
        var left: number = center - partOfStep;
        var right: number = center + partOfStep;

        return (x: number) => {
            if(left <= x && x < center1){
                return (x - left) / (center1 - left);
            } else if(center1 <= x && x < center2) {
                return 1;
            } else if(center2 <= x && x < right) {
                return (x - right) / (center2 - right);
            } else {
                return 0;
            }
        };
    }

    private _getParabaloidFunc(termNumber: number, params: InputParamConfig): (x: number) => number {
        var start: number ;
        var end: number;
        var min = params.minValue;
        var max = params.maxValue;
        var step = this._getStepSize(min, max);
        var partOfStep = step / 2 + step / 7;

        var center: number = min + step * termNumber;
        start = center - partOfStep;
        end = center + partOfStep;

        var b = (end - start) / 2;
        var a = start + b;

        return (x: number) => {
            if((a-b)<= x && x < (a+b)){
                return 1 - Math.pow(((x - a) / b), 2);
            } else {
                return 0;
            }            
        };
    }

    private _getGaussianFunc(termNumber: number, params: InputParamConfig): (x: number) => number {
        var min = params.minValue;
        var max = params.maxValue;
        var step = this._getStepSize(min, max);
        var partOfStep = step / 2 + step / 7;
        var center: number = min + step * termNumber;

        console.log(step);
        console.log(partOfStep);
        console.log(center);

        return (x) => Math.exp(-1 * Math.pow((x - center) / partOfStep, 2));
    }

    private _getStepSize(min: number, max: number): number {
        var termsNum = this.termsNumber;
        return (max - min) / (termsNum - 1);
    }

    private _getMSE(): number {
        var sum = 0;
        for(let item of this.observeTable) {
            sum += Math.pow(item.Z - item.F_x, 2);
        }

        return Math.sqrt(sum) / this.observeTable.length;
    }

    private _getAAE(): number {
        var sum = 0;
        for(let item of this.observeTable) {
            sum += Math.abs(item.Z - item.F_x);
        }

        return sum / this.observeTable.length;
    }

    private _getME(): number {
        return this.observeTable.map(item => Math.abs(item.Z - item.F_x)).reverse()[0];
    }
}

export class InputParamConfig {
    public name: string;
    public minValue: number;
    public maxValue: number;
}

export enum FunctionType {
    Triangular = 1,
    Trapezoid = 2,
    Parabolic = 3,
    Gaussian = 4
}

export interface IRule {
    X: number;
    Y: number;
    Z: number;
}

export interface IObserveItem extends IRule {
    F_x: number;
}